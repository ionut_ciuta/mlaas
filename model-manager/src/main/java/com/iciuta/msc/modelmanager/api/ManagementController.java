package com.iciuta.msc.modelmanager.api;

import com.iciuta.msc.modelmanager.docker.DockerManager;
import com.iciuta.msc.modelmanager.tools.PortProvider;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
public class ManagementController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DockerManager dockerManager;

    @Autowired
    private PortProvider portProvider;

    @Autowired
    private EurekaClient eurekaClient;

    @PostMapping(value = "/start")
    public ResponseEntity<String> start(@RequestHeader String user, @RequestParam String model) throws Exception {
        String containerId = dockerManager.startContainer(user + "-" + model, model);
        return ResponseEntity.ok(containerId);
    }

    @GetMapping(value = "/train")
    public ResponseEntity<String> train(@RequestParam String container, @RequestParam String model) {
        InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka(container, false);
        String baseUrl = instanceInfo.getHomePageUrl();
        ResponseEntity<String> response = restTemplate.exchange(
                baseUrl.concat("/train/").concat(model),
                HttpMethod.GET,
                null,
                String.class);
        return ResponseEntity.ok(response.getBody());
    }
}
