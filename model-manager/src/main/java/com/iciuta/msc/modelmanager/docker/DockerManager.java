package com.iciuta.msc.modelmanager.docker;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateNetworkResponse;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.Network;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.iciuta.msc.modelmanager.tools.PortProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class DockerManager {

    @Value("${modelManager.docker.modelContainerImage}")
    private String modelContainerImage;

    @Value("${modelManager.docker.network}")
    private String dockerNetwork;

    @Value("${modelManager.docker.eurekaHost}")
    private String eurekaHost;

    @Value("${modelManager.docker.eurekaPort}")
    private String eurekaPort;

    @Value("${modelManager.docker.modelExposedPort}")
    private Integer modelExposedPort;

    @Value("${modelManager.docker.modelEntrypointCmd}")
    private String modelEntrypointCmd;

    @Autowired
    private PortProvider portProvider;

    private DockerClient dockerClient;

    @PostConstruct
    public void setup() {
        DockerClientConfig clientConfig = DefaultDockerClientConfig
                .createDefaultConfigBuilder()
                .build();

        this.dockerClient =  DockerClientBuilder
                .getInstance(clientConfig)
                .build();
    }

    public String startContainer(String modelContainerName, String modelName) throws Exception {
        int modelHostPort = portProvider.getAvailablePort();

        String[] entrypointArgs = {
                modelEntrypointCmd,
                eurekaHost,
                eurekaPort,
                modelContainerName,
                modelHostPort + ""
        };

        log.info("Starting container:\n\t- named: {}\n\t- from image: {}\n\t- with name: {}\n\t- on host port: {}" +
                "\n\t- with entrypoint args: {}\n\t- on network: {}\n", modelContainerName, modelContainerImage,
                modelName, modelHostPort, entrypointArgs, dockerNetwork);

        Ports portBindings = new Ports();
        portBindings.bind(
                ExposedPort.tcp(modelExposedPort),
                Ports.Binding.bindPort(modelHostPort)
        );

        HostConfig hostConfig = HostConfig
                .newHostConfig()
                .withPortBindings(portBindings);

        Network network = dockerClient
                .inspectNetworkCmd()
                .withNetworkId(dockerNetwork)
                .exec();

        String containerId  = dockerClient
                .createContainerCmd(modelContainerImage)
                .withEntrypoint(entrypointArgs)
                .withName(modelContainerName)
                .withHostConfig(hostConfig)
                .exec()
                .getId();

        dockerClient.connectToNetworkCmd()
                .withNetworkId(network.getId())
                .withContainerId(containerId)
                .exec();

        dockerClient.startContainerCmd(containerId).exec();

        log.info("\nContainer {} started with id {}!\n", modelContainerName, containerId);
        return containerId;
    }
}
