package com.iciuta.msc.modelmanager;

import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationListener;
import org.springframework.web.client.RestTemplate;

@EnableDiscoveryClient
@SpringBootApplication
public class ModelManagerApplication implements ApplicationListener<ApplicationStartedEvent> {
	private static int retries = 0;

	@Value("${eureka.client.service-url.defaultZone}")
	private String eurekaDefaultZone;

	@Autowired
	private EurekaClient eurekaClient;

	public static void main(String[] args) {
		try {
			Thread.sleep(15 * 1000);
			SpringApplication.run(ModelManagerApplication.class, args);
 		} catch (InterruptedException e) {
			System.err.println("Something went wrong while trying to start up the application! Retrying...");
			if(retries < 3) {
				retries++;
				System.err.println("Retry " + retries);
				main(args);
			} else {
				System.err.println("Max retries exceed. System will now exit.");
				System.exit(-1);
			}
		}
	}

	@Override
	public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
//		System.out.println("Eureka default zone " + eurekaDefaultZone + " " + eurekaClient.getEurekaClientConfig().getEurekaServerServiceUrls("default-zone"));
//		RestTemplate restTemplate = new RestTemplate();
//		String response = restTemplate.getForEntity(eurekaDefaultZone + "/test", String.class).getBody();
//		System.out.println("Message from DS: " + response);
	}
}
