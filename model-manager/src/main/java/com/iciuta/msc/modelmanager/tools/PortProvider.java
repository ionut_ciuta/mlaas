package com.iciuta.msc.modelmanager.tools;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.net.ServerSocket;

@Slf4j
@Component
public class PortProvider {

    public int getAvailablePort() throws Exception {
        try(ServerSocket socket = new ServerSocket(0)) {
            int port = socket.getLocalPort();
            log.info("Available port found {}", port);
            return port;
        }
    }
}
