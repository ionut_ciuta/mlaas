#!/bin/bash

echo "Installing requirements"
pip install -r requirements.txt

echo
echo "Running app"
echo

python app.py $1 $2 $3 $4