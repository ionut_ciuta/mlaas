import sys
from flask import Flask
import py_eureka_client.eureka_client as eureka_client

app = Flask(__name__)

@app.route('/train/<model>')
def train(model):
    return 'Train model > {}'.format(model)

@app.route('/predict/<model>')
def predict(model):
    return 'Predict using model > {}'.format(model)

def register_with_eureka(discovery_server_host, discovery_server_port, service_name, service_port):
	print('Registering %s:%i to %s:%i' % (service_name, service_port, discovery_server_host, discovery_server_port));
	discovery_server = 'http://%s:%s/eureka' % (discovery_server_host, discovery_server_port)
	eureka_client.init(
		eureka_server=discovery_server,
		app_name=service_name,
		instance_port=service_port
	)

if __name__ == '__main__':
	print('Model args: %s' % str(sys.argv))
	if len(sys.argv) != 5:
		raise Exception('Run as: app.py <discovery_server_host> <discovery_server_port> <service_name> <service_port>')
	register_with_eureka(
		discovery_server_host=sys.argv[1],
		discovery_server_port=int(sys.argv[2]),
		service_name=sys.argv[3],
		service_port=int(sys.argv[4])
	)
	app.run(
		host='0.0.0.0',
		port=int(sys.argv[4])
	)