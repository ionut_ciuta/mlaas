#### Notebooks

Install virtualenv:
`pip3 install --user --upgrade virtualenv`

Create the virtual environment:
`virtualenv env`

Activate the environment:
`source env/bin/activate`

Intall requirements:
`pip3 install -r requirements.txt`

